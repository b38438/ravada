# Change Log


**Implemented enhancements:**

- Remote nodes support
- Shared storage remote nodes [\#1014]
- Manage Network interfaces [\#1011]
- New permission to rename the machine [\#986]
- Manage CDROM [\#973]
- Start and Stop nodes [\#953]
- Shutdown machines when disabling node enhancement [\#947]
- Upgrade Bootstrap to 4.x [\#886]
- Allow volatile machines in remote nodes [#865]
- Remote machines sync back with no shared storage [\#583]
- Add more storage to a VM [\#495]
- Sync clock after hibernate [\#466]
- Show the Internal IP [\#465]
- Support TLS encryption channels in SPICE [\#249]
- Allow add volumes to a virtual machine [\#198]
- Check free disk space [\#10]
